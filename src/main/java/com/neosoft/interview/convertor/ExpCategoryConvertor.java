package com.neosoft.interview.convertor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.neosoft.interview.dto.CategoryDto;
import com.neosoft.interview.dto.ExperienceDto;
import com.neosoft.interview.entity.SubjectCategory;
import com.neosoft.interview.entity.SubjectExperience;
import com.neosoft.interview.service.SubjectCategoryService;



@Component
public class ExpCategoryConvertor {
	
	@Autowired
	SubjectCategoryService subjectCategoryService;
	
	public SubjectExperience dtoToEntity(ExperienceDto experienceDto) {
		SubjectExperience subjectExperience=new SubjectExperience();
		
		try {
			
			subjectExperience.setExpTitle(experienceDto.getJobTitle());
			
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return subjectExperience;
		
	}

	
	public List<SubjectCategory> listDToToListEntity(List<CategoryDto> categoryDtos,SubjectExperience subjectExperience){
		
		List<SubjectCategory> subjectCategories= new ArrayList<SubjectCategory>();
		try {
			for(CategoryDto categoryDto:categoryDtos) 
			{
				SubjectCategory subjectCategory=dtoToEntity(categoryDto,subjectExperience);
				subjectCategories.add(subjectCategory);
			}
			
			
			} catch (Exception e) {
			e.printStackTrace();
			}
		return subjectCategories;
		
	}
	
public SubjectCategory  dtoToEntity(CategoryDto categoryDto, SubjectExperience subjectExperience) {
		
	SubjectCategory subjectCategory=new SubjectCategory();
	subjectCategory.setSubCategoryTitle(categoryDto.getCategoryName());
	subjectCategory.setSubjectExperience(subjectExperience);
	
	
	return subjectCategory;
}

}
