package com.neosoft.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManageInterviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManageInterviewApplication.class, args);
	}

}
