package com.neosoft.interview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neosoft.interview.entity.SubjectsubCategory;
import com.neosoft.interview.repository.SubjectSubCategoryRepository;

@Service
public class SubjectSubCategoryServiceImpl implements SubjectSubCategoryService{
	
	@Autowired
	SubjectSubCategoryRepository jobSubCategoryRepository;

	@Override
	public SubjectsubCategory save(SubjectsubCategory jobSubCategory) {
		// TODO Auto-generated method stub
		return jobSubCategoryRepository.save(jobSubCategory);
	}

	@Override
	public SubjectsubCategory findById(Integer id) {
		// TODO Auto-generated method stub
		return jobSubCategoryRepository.findById(id).orElse(null);
	}

}
