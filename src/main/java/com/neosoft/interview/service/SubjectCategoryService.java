package com.neosoft.interview.service;

import org.springframework.stereotype.Service;

import com.neosoft.interview.entity.SubjectCategory;

@Service
public interface SubjectCategoryService {
	
	SubjectCategory save(SubjectCategory jobCategory);
	
	SubjectCategory getById(Integer id);

}
