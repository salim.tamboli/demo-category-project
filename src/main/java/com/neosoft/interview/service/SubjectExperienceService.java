package com.neosoft.interview.service;

import org.springframework.stereotype.Service;

import com.neosoft.interview.entity.SubjectExperience;

@Service
public interface SubjectExperienceService {
	
	 SubjectExperience save(SubjectExperience jobProfile);
	
     SubjectExperience  getByid(Integer id);
}
