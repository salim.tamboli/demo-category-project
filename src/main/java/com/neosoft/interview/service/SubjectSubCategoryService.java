package com.neosoft.interview.service;

import org.springframework.stereotype.Service;

import com.neosoft.interview.entity.SubjectsubCategory;

@Service
public interface SubjectSubCategoryService {
	
	SubjectsubCategory save(SubjectsubCategory jobSubCategory);
	
	SubjectsubCategory findById(Integer id);

}
