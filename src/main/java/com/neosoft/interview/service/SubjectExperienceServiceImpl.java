package com.neosoft.interview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neosoft.interview.entity.SubjectExperience;
import com.neosoft.interview.repository.SubjectExperienceRepository;

@Service
public class SubjectExperienceServiceImpl implements SubjectExperienceService{
	
	@Autowired
	SubjectExperienceRepository jobProfileRepository;

	@Override
	public SubjectExperience save(SubjectExperience jobProfile) {
		// TODO Auto-generated method stub
		return jobProfileRepository.save(jobProfile);
	}

	@Override
	public SubjectExperience getByid(Integer id) {
		// TODO Auto-generated method stub
		return jobProfileRepository.findById(id).orElse(null);
	}
	
	

}
