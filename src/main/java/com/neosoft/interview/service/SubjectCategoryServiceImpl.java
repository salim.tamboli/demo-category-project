package com.neosoft.interview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neosoft.interview.entity.SubjectCategory;
import com.neosoft.interview.repository.SubjectCategoryRepository;

@Service
public class SubjectCategoryServiceImpl implements SubjectCategoryService{
	

	@Autowired
	SubjectCategoryRepository jobCategoryRepository;

	@Override
	public SubjectCategory save(SubjectCategory jobCategory) {
		// TODO Auto-generated method stub
		return jobCategoryRepository.save(jobCategory);
	}

	@Override
	public SubjectCategory getById(Integer id) {
		// TODO Auto-generated method stub
		return jobCategoryRepository.findById(id).orElse(null);
	}
	
	
	

}
