package com.neosoft.interview.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="category_profile")
public class SubjectCategory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String subCategoryTitle;
	
	@ManyToOne
	@JoinColumn(name = "expTitle")
	private SubjectExperience subjectExper;
	
	@OneToMany(mappedBy = "subjectCategory", cascade = CascadeType.ALL)
	private List<SubjectsubCategory> subjectSubCategories;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubCategoryTitle() {
		return subCategoryTitle;
	}

	public void setSubCategoryTitle(String subCategoryTitle) {
		this.subCategoryTitle = subCategoryTitle;
	}

	public SubjectExperience getSubjectExperience() {
		return subjectExper;
	}

	public void setSubjectExperience(SubjectExperience subjectExperience) {
		this.subjectExper = subjectExperience;
	}

	public List<SubjectsubCategory> getJobSubCategories() {
		return subjectSubCategories;
	}

	public void setJobSubCategories(List<SubjectsubCategory> jobSubCategories) {
		this.subjectSubCategories = jobSubCategories;
	}
	
	

}
