package com.neosoft.interview.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="Experience_profile")
public class SubjectExperience {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String expTitle;
	
	@OneToMany(mappedBy = "subjectExper", cascade = CascadeType.ALL)
	private List<SubjectCategory> subjectCategories;
	
	@OneToMany(mappedBy = "subjectExperience", cascade = CascadeType.ALL)
	private List<SubjectsubCategory> subjectSubCategories;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExpTitle() {
		return expTitle;
	}

	public void setExpTitle(String expTitle) {
		this.expTitle = expTitle;
	}

	public List<SubjectCategory> getSubjectCategories() {
		return subjectCategories;
	}

	public void setSubjectCategories(List<SubjectCategory> subjectCategories) {
		this.subjectCategories = subjectCategories;
	}

	public List<SubjectsubCategory> getSubjectSubCategories() {
		return subjectSubCategories;
	}

	public void setSubjectSubCategories(List<SubjectsubCategory> subjectSubCategories) {
		this.subjectSubCategories = subjectSubCategories;
	}
	

}
