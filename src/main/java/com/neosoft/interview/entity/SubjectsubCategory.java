package com.neosoft.interview.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;




@Entity
@Table(name="job_sub_category")
public class SubjectsubCategory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String subCategoryName;
	
	@ManyToOne
	@JoinColumn(name = "expTitle")
	private SubjectExperience subjectExperience;
	
	@ManyToOne
	@JoinColumn(name = "categoryTitle")
	private SubjectCategory subjectCategory;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public SubjectExperience getSubjectExperience() {
		return subjectExperience;
	}

	public void setSubjectExperience(SubjectExperience subjectExperience) {
		this.subjectExperience = subjectExperience;
	}

	public SubjectCategory getSubjectCategory() {
		return subjectCategory;
	}

	public void setSubjectCategory(SubjectCategory subjectCategory) {
		this.subjectCategory = subjectCategory;
	}

	
}
