package com.neosoft.interview.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;

import com.neosoft.interview.convertor.ExpCategoryConvertor;
import com.neosoft.interview.dto.CategoryDto;
import com.neosoft.interview.dto.ExperienceDto;
import com.neosoft.interview.dto.SubCategoryDto;
import com.neosoft.interview.entity.SubjectCategory;
import com.neosoft.interview.entity.SubjectExperience;
import com.neosoft.interview.repository.SubjectExperienceRepository;
import com.neosoft.interview.service.SubjectExperienceService;


@Controller
public class CategoryController {
	
	private static final Logger logger  = LoggerFactory.getLogger(CategoryController.class);
	
	@Autowired
	ExpCategoryConvertor expCategoryConvertor;
	
	@Autowired
	SubjectExperienceService subjectExperienceleService;
	
	@Autowired
	SubjectExperienceRepository subjectExperienceRepository;
	
	@RequestMapping(value= "/add_subject_experience", method = RequestMethod.GET)
	public ModelAndView addJobProfilewithCategories(@ModelAttribute ExperienceDto experienceDto){
		logger.info("--add_Experience add_job_Experience-");
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("jobProfileDto", experienceDto);
		modelAndView.setViewName("exp-category/job-profile");
		modelAndView.setViewName("exp-category/job-sub-category");
		//modelAndView.setViewName("redirect:add_update_SubjectExp");
		return modelAndView;
	}
	
	@RequestMapping(value="/add_update_SubjectExp", method=RequestMethod.POST)
	public ModelAndView addUpdateSubjectSubCategories(@ModelAttribute ExperienceDto experienceDto, final BindingResult result ) {
		logger.info("--  add_update_job_profile  --");
		
		ModelAndView modelAndView = new ModelAndView();
		SubjectExperience subjectExperience=null;
		try {
			subjectExperience=expCategoryConvertor.dtoToEntity(experienceDto);
		List<SubjectCategory> subjectCategory=null;
		subjectCategory=expCategoryConvertor.listDToToListEntity(experienceDto.getCategoryDtos(), subjectExperience);
		subjectExperience.setSubjectCategories(subjectCategory);
	
		subjectExperienceleService.save(subjectExperience);
		modelAndView.addObject("token",subjectExperience.getId().toString());
		modelAndView.setViewName("exp-category/job-sub-category");
		//modelAndView.setViewName("redirect:show_profile_category");
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}		
		return modelAndView;
	}
	
	
	@RequestMapping(value= "/show_profile_category", method = RequestMethod.GET)
	public ModelAndView doneMessage(@ModelAttribute ExperienceDto experienceDto){
		logger.info("--Done Message Done Message-");
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("jobProfileDto", experienceDto);
		modelAndView.setViewName("exp-category/message");		
		return modelAndView;
	}
	
}
