package com.neosoft.interview.dto;

import java.util.List;

import javax.persistence.Entity;


public class CategoryDto {
	
	private String id;
	
	private String categoryName;
	
	private List<SubCategoryDto> subCategoryDtos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<SubCategoryDto> getSubCategoryDtos() {
		return subCategoryDtos;
	}

	public void setSubCategoryDtos(List<SubCategoryDto> subCategoryDtos) {
		this.subCategoryDtos = subCategoryDtos;
	}

}
