package com.neosoft.interview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.interview.entity.SubjectExperience;

@Repository
public interface SubjectExperienceRepository extends JpaRepository<SubjectExperience, Integer>{
	
	

}
