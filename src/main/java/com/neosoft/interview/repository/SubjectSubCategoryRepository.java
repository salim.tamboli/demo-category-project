package com.neosoft.interview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.interview.entity.SubjectsubCategory;

@Repository
public interface SubjectSubCategoryRepository extends JpaRepository<SubjectsubCategory, Integer>{

}
