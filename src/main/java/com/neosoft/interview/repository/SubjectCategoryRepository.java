package com.neosoft.interview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.interview.entity.SubjectCategory;


@Repository
public interface SubjectCategoryRepository extends JpaRepository<SubjectCategory, Integer>{
	
	

}
