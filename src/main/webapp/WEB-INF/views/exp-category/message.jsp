<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Profile With Category</title>
</head>
<body>
<div>Done</div>

<div class="content-body">
		<div class="content-wrapper">
			<div class="content-header row">
				<div class="content-header-left col-md-8 col-xs-12"></div>

			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="card">

						<div class="card-body collapse in">
							<div class="card-block">



								<form:form class="form" method="POST"
									action="${context}/add_subject_experience"
									modelAttribute="jobProfileDto">



									<div class="form-body">
										<div class="row">
											<div class="form-group col-md-6 mb-2 ">
												<p class="control-label ">Job Title</p>

												<form:input maxlength="200" type="text" path="jobTitle"
													class="form-control" placeholder="Job Title" />

											</div>
										</div>

									</div>
						
<%-- <table border="1">

		

		<c:forEach var="category" items="${listEmp}" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<td>${category.subCategoryTitle}</td>
				

			</tr>
		</c:forEach>
	</table>--%>


							</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>


	


</body>